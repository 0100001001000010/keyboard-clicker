#!/usr/bin/env python

# Messages that appear in the UI
class UI_MESSAGES:
    jsonError = 'Could not read configuration file. Filesystem error or invalid JSON.'
    testButtonClicked = 'This will teleport your mouse cursor and help you make sure it will remain over the appropriate areas of the screen.'
    unreachableCodeError = 'Serious internal error indicating a bug. Please report this immediately.'
    windowTitle = 'Keyboard Clicker'
    presetLabel = 'Preset: '
    testButtonLabel = 'Test'
    dependError = 'Could not load required dependency '

#Attempt to load GUI library, show terminal error if failed.
try:
    from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QComboBox, QVBoxLayout, QHBoxLayout, QPushButton, QMessageBox
except ImportError as error:
    print(UI_MESSAGES.dependError + error.name)
    raise SystemExit(1)
#Attempt to load remaining libraries and show errors if they failed.
try:
    import pyautogui
    import pyxhook
    import time
    import os
    import json
except ImportError as error:
    print(UI_MESSAGES.dependError + error.name)
    app = QApplication([])
    window = QWidget()
    window.setWindowTitle(UI_MESSAGES.windowTitle)
    layout = QHBoxLayout()
    errorMsg = QLabel(UI_MESSAGES.dependError + error.name)
    layout.addWidget(errorMsg)
    window.setLayout(layout)
    window.show()
    app.exec_()  # Start running the GUI
    raise SystemExit(1)

# Function to show message boxes
def popup(message):
    alert = QMessageBox()
    alert.setText(message)
    alert.exec_()


# Set up the keyboard modes
# Start with a default configuration
keyModes = [
    {
        'name': 'Kahoot',
        'helpText': '1: Top left\n2: Top right\n3: Bottom left\n4: Bottom right',
        'keys': {
            '1': [0.25, 0.25],
            '2': [0.75, 0.25],
            '3': [0.25, 0.75],
            '4': [0.75, 0.75]
        }
    },
    {
        'name': 'Quizizz',
        'helpText': '1: Far left\n2: Mid left\n3: Mid right\n4: Far right',
        'keys': {
            '1': [0.125, 0.8],
            '2': [0.375, 0.8],
            '3': [0.625, 0.8],
            '4': [0.875, 0.8]
        }
    }
]
CONFIG_FILE_PATH = os.path.join(os.environ['HOME'], '.keyboardClickerKeys')
try:
    if os.path.isfile(CONFIG_FILE_PATH):
        configFile = open(CONFIG_FILE_PATH).read()
        configFromFile = json.JSONDecoder().decode(configFile)
        keyModes += configFromFile
except:
    app = QApplication([])
    popup(UI_MESSAGES.jsonError)
    raise SystemExit(1)
keyboardMode = 0

# Define event handlers
def handleKbd(event):
    for possibleKey in keyModes[keyboardMode]['keys']:
        if event.Key == possibleKey:
            pyautogui.click(keyModes[keyboardMode]['keys'][possibleKey]
                            [0] * width, keyModes[keyboardMode]['keys'][possibleKey][1] * height)


def handleTestButton():
    popup(UI_MESSAGES.testButtonClicked)
    for key in keyModes[keyboardMode]['keys']:
        pyautogui.moveTo(keyModes[keyboardMode]['keys']
                         [key][0] * width, keyModes[keyboardMode]['keys'][key][1] * height)
        time.sleep(1)


def changeKbdMode():
    global keyboardMode
    keyboardMode = presetBox.currentIndex()
    popup(keyModes[keyboardMode]['helpText'])


# Set up keyboard and mouse libraries
hm = pyxhook.HookManager()
hm.KeyDown = handleKbd
hm.HookKeyboard()
hm.start()
width, height = pyautogui.size()

# Set up graphical user interface
app = QApplication([])
window = QWidget()
window.setWindowTitle(UI_MESSAGES.windowTitle)
layout = QHBoxLayout()
presetBoxLabel = QLabel(UI_MESSAGES.presetLabel)
presetBox = QComboBox()
testButton = QPushButton(UI_MESSAGES.testButtonLabel)
layout.addWidget(presetBoxLabel)
layout.addWidget(presetBox)
layout.addWidget(testButton)
window.setLayout(layout)
window.show()
testButton.clicked.connect(handleTestButton)
for keyMode in keyModes:
    presetBox.addItem(keyMode['name'])
presetBox.activated.connect(changeKbdMode)

app.exec_()  # Start running the GUI
hm.cancel()  # When the GUI exits, stop the keyboard listener
